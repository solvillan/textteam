###TextTeam
TextTeam is a textbased, co-op game, inspired by SpaceTeam by Henry Smith.
It is licensed under the MIT License, which can be viewed [here](https://bitbucket.org/solvillan/textteam/src/6d172600e42a776b97231701cb936ae76a32a8e3/LICENSE.md?at=master).