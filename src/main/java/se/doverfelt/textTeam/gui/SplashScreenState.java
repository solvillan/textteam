/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui;

import org.newdawn.slick.*;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.state.transition.Transition;
import se.doverfelt.textTeam.Main;
import twl.slick.BasicTWLGameState;
import twl.slick.RootPane;

public class SplashScreenState extends BasicTWLGameState {
    private Image splash;

    private long timeStamp;

    private boolean startSound;

    private Sound startupSound;

    @Override
    protected RootPane createRootPane() {
        RootPane rp = super.createRootPane();
        rp.setTheme("-defaults");

        return rp;
    }

    @Override
    protected void layoutRootPane() {

    }

    @Override
    public int getID() {
        return 0;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        splash = new Image("/res/gasolgasoft-8bit.png").getScaledCopy(gameContainer.getHeight(), gameContainer.getHeight());
        startupSound = new Sound(SplashScreenState.class.getResource("/res/gasolgasoft.ogg"));
        startupSound.play(0.8f, 2);
        timeStamp = System.currentTimeMillis();
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        graphics.drawImage(splash, (gameContainer.getWidth()/2)-(splash.getWidth()/2), (gameContainer.getHeight()/2)-(splash.getHeight()/2));
        graphics.drawString("TextTeam " + Main.VERSION, 10,25);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        if (timeStamp + 5000 <= System.currentTimeMillis()) {
            MainMenuState.bgMusic.loop(1.25f, 2f);
            stateBasedGame.enterState(1, new FadeOutTransition(), new FadeInTransition());
        }
    }
}
