/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui;

import de.matthiasmann.twl.Button;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import se.doverfelt.textTeam.Main;
import twl.slick.BasicTWLGameState;
import twl.slick.RootPane;

public class MainMenuState extends BasicTWLGameState {

    private Button host;
    private Button quit;

    private Image bg;

    private StateBasedGame sbg;

    public static Music bgMusic;

    @Override
    public int getID() {
        return 1;
    }

    @Override
    protected RootPane createRootPane() {
        RootPane rp = super.createRootPane();
        rp.setTheme("-defaults");

        host = new Button("Host Game");
        host.addCallback(new Runnable() {
            @Override
            public void run() {
                sbg.enterState(2);
            }
        });

        quit = new Button("Quit Game");
        quit.addCallback(new Runnable() {
            @Override
            public void run() {
                bgMusic.stop();
                System.exit(0);
            }
        });

        rp.add(host);
        rp.add(quit);

        return rp;
    }

    @Override
    protected void layoutRootPane() {
        host.adjustSize();
        host.setPosition(50, 50);
        quit.adjustSize();
        quit.setPosition(host.getX() + host.getWidth() + 10, 50);
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        bg = new Image("/res/logo.png");
        bgMusic = new Music(Main.class.getResource("/res/Fur_Elise.ogg"));
        sbg = stateBasedGame;
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        graphics.drawImage(bg, 0, 0, gameContainer.getWidth(), gameContainer.getHeight(), 0, 0, bg.getWidth(), bg.getHeight());
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {

    }
}
