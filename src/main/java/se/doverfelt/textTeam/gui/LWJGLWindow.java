/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import twl.slick.TWLStateBasedGame;

import java.net.URL;

public class LWJGLWindow extends TWLStateBasedGame {

    private String name;
    private MainMenuState mainMenuState;
    private GameSceneState gameSceneState;
    private SplashScreenState splashScreenState;

    public LWJGLWindow(String name) {
        super(name);
        this.name = name;
    }

    @Override
    protected URL getThemeURL() {
        return LWJGLWindow.class.getResource("/res/theme.xml");
    }

    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {

        splashScreenState = new SplashScreenState();
        addState(splashScreenState);

        mainMenuState = new MainMenuState();
        addState(mainMenuState);

        gameSceneState = new GameSceneState();
        addState(gameSceneState);

        enterState(0, new FadeInTransition(), null);
    }
}
