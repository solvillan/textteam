/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui;

import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.ProgressBar;
import de.matthiasmann.twl.TextArea;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import twl.slick.BasicTWLGameState;
import twl.slick.RootPane;

public class GameSceneState extends BasicTWLGameState {

    private Image bg;

    private ProgressBar time;
    private TextArea missions;
    private Button quit;

    private GameContainer gc;

    @Override
    protected RootPane createRootPane() {
        RootPane rp = super.createRootPane();
        rp.setTheme("-defaults");

        time = new ProgressBar();
        missions = new TextArea();

        quit = new Button("Quit Game");
        quit.addCallback(new Runnable() {
            @Override
            public void run() {
                MainMenuState.bgMusic.stop();
                System.exit(0);
            }
        });

        rp.add(time);
        rp.add(missions);
        rp.add(quit);
        return rp;
    }

    @Override
    protected void layoutRootPane() {
        time.adjustSize();
        time.setPosition(10, 10);

        missions.adjustSize();
        missions.setPosition(10, 50);

        quit.adjustSize();
        quit.setPosition(gc.getWidth() - (quit.getWidth() + 10), gc.getHeight() - (quit.getHeight() + 10));
    }

    @Override
    public int getID() {
        return 2;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        bg = new Image("/res/logo.png");
        gc = gameContainer;
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        graphics.drawImage(bg, 0, 0, gameContainer.getWidth(), gameContainer.getHeight(), 0, 0, bg.getWidth(), bg.getHeight());
        graphics.drawString("Hello, world!", (gameContainer.getWidth()/2)-60, gameContainer.getHeight()/2);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {

    }
}
