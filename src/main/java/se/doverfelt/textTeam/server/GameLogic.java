/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import se.doverfelt.textTeam.Main;
import se.doverfelt.textTeam.client.ClientLogic;
import se.doverfelt.textTeam.packets.Handshake;
import se.doverfelt.textTeam.packets.Message;
import se.doverfelt.textTeam.packets.Quit;
import se.doverfelt.textTeam.packets.TimeLimit;

import java.util.HashMap;

public class GameLogic extends Listener implements Runnable {

    HashMap<Integer, ClientInfo> clients = new HashMap<Integer, ClientInfo>();
    int lastId = 0;
    private boolean shouldPlay = false;
    private int timeLimit;

    @Override
    public void received(Connection connection, Object object) {

        if (object instanceof Handshake) {
            Handshake handshake = (Handshake)object;
            if (handshake.type != Handshake.REQUEST) return;

            lastId++;

            Handshake response = new Handshake();
            response.type = Handshake.RESPONSE;
            response.id = lastId;

            connection.sendTCP(response);

            clients.put(lastId, new ClientInfo(lastId, connection.getRemoteAddressTCP().getHostName()));

            Message message = new Message();
            message.msg = connection.getRemoteAddressTCP().getHostName() + " id " + lastId + " has connected!";

            GameServer.server.sendToAllTCP(message);

        }

        if (object instanceof Message) {
            Message msg = (Message)object;
            String[] in = msg.msg.split(" ");
            if (in[0].toLowerCase().equals("say")) {
                Message response = new Message();
                String out = "";
                for (int i = 1; i < in.length; i++) {
                    out += in[i] + " ";
                }
                response.msg = msg.ID + ": " + out;
                GameServer.server.sendToAllTCP(response);

                return;
            }

            if (in[0].toLowerCase().equals("quit")) {
                Quit quit = new Quit();
                quit.reason = "Server Closed";
                GameServer.server.sendToAllTCP(quit);
                return;
            }

            if (in[0].toLowerCase().equals("play") || in[0].toLowerCase().equals("start")) {
                timeLimit = Integer.parseInt(in[1]);
                shouldPlay = true;
                Message response = new Message();
                response.msg = "Game Starts! Time: " + Float.parseFloat(in[1])/1000 + "s";
                GameServer.server.sendToAllTCP(response);
                return;
            }

            String command = "";
            try {
                command = clients.get(msg.ID).getPanels().get(in[0]).getCommand();
            } catch (NullPointerException e) {
                Main.window.gameServer.logError(e);
                e.printStackTrace();
            }
            if (command.isEmpty() || command == null || in.length < 1) return;

            try {
                clients.get(msg.ID).getPanels().get(command).setValue(in[1]);
                Main.window.gameScene.missions.append(command + " set to " + in[1]);
            } catch (ArrayIndexOutOfBoundsException e) {
                Main.window.gameScene.missions.append("[SERVER] You didn't specify any arguments!");
            }

        }

    }

    @Override
    public void run() {
        while (true) {
            if (shouldPlay) {
                TimeLimit time = new TimeLimit();
                time.millis = timeLimit;
                GameServer.server.sendToAllTCP(time);
                shouldPlay = false;
            }
            Thread.yield();
        }
    }
}
