/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam;

import com.esotericsoftware.kryo.Kryo;
import se.doverfelt.textTeam.packets.*;
import se.doverfelt.textTeam.server.Panels;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by solvillan on 2014-03-02.
 */
public class Utils {

    private static ArrayList<String> usedPanels = new ArrayList<>();
    public static HashMap<String, Panels> allPanels = new HashMap<>();

    public static void registerClasses(Kryo kryo) {
        kryo.register(Handshake.class);
        kryo.register(Message.class);
        kryo.register(TimeLimit.class);
        kryo.register(TimeLimitExpired.class);
        kryo.register(Quit.class);
    }

    public static int randomInt(int min, int max) {
        Random random = new Random();
        int gen = random.nextInt(max + 1);

        while (gen < min) {
            gen = random.nextInt(max + 1);
        }

        return gen;
    }

    public static String randomString(int length) {
        String out = "";
        Random random = new Random();

        for (int j = 0; j < length; j++) {
            out += (char)randomInt(97, 122);
        }

        return out;
    }


    public static HashMap<String, Panels> readPanels2(int id) throws IOException {
        HashMap<String, Panels> panels = new HashMap<String, Panels>();

        File inFile = new File("commands2.txt");
        BufferedReader reader = Files.newBufferedReader(inFile.toPath(), Charset.defaultCharset());

        String in = "";
        String[] inArray = new String[255];
        String line = null;

        int i = 0;
        while ((line = reader.readLine()) != null) {
            in += line + "\n";
            inArray[1] = line;
            i++;
        }

        System.out.println(in);

        return panels;
    }

    public static HashMap<String, Panels> readPanels() {

        HashMap<String, Panels> panels = new HashMap<String, Panels>();

        File commandFile = new File("commands2.txt");

        try (BufferedReader reader = Files.newBufferedReader(commandFile.toPath(), Charset.defaultCharset())) {
            String line = null;
            ArrayList<String> commands = new ArrayList<>();
            ArrayList<ArrayList<String>> values = new ArrayList<>();
            ArrayList<String> desc = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                String[] splitted = line.split(";");

                commands.add(splitted[0]);
                desc.add(splitted[2]);

                String[] value = splitted[1].split(",");
                ArrayList<String> aValue = new ArrayList<>();
                for (String i : value) {
                    aValue.add(i);
                }
                values.add(aValue);
            }

            for (int i = 0; i < commands.size(); i++) {
                panels.put(commands.get(i), new Panels(commands.get(i), desc.get(i), values.get(i)));
            }

            System.out.println(commands.toString());
            System.out.println(values.toString());
            System.out.println(desc.toString());



        } catch (IOException e) {
            e.printStackTrace();
        }

        return panels;

/*
        HashMap<String, Panels> spanels = new HashMap<String, Panels>();

        Random random = new Random();

        for (int i = 0; i < 5; i++) {
            String name = randomString(5);
            Message msg = new Message();
            msg.msg = name;
            GameServer.server.sendToTCP(id, msg);
            spanels.put(name, new Panels(name, desc.get(i), values.get(i)));
        }

        return spanels;*/
    }



    public static HashMap<String, Panels> getRandomFreePanels() {
        if (allPanels == null || allPanels.isEmpty()) {
            Main.window.gameServer.logError(new Exception("Panels are NULL, check if commands.txt is empty!"));
        }

        HashMap<String, Panels> panels = new HashMap<>();

        int i = 0;
        while (i < 3) {
            for (String s : allPanels.keySet()) {
                for (String d : usedPanels) {
                    if (!s.equals(d)) {
                        panels.put(s, allPanels.get(s));
                        usedPanels.add(s);
                        i++;
                        System.out.println("Added panel " + s + "to usedPanels!");
                    }
                }
            }
        }

        return null;
    }
}
