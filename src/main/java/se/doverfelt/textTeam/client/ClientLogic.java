/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.client;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import se.doverfelt.textTeam.Main;
import se.doverfelt.textTeam.packets.Handshake;
import se.doverfelt.textTeam.packets.Message;
import se.doverfelt.textTeam.packets.Quit;
import se.doverfelt.textTeam.packets.TimeLimit;

public class ClientLogic extends Listener {

    public static int ID;

    @Override
    public void received(Connection connection, Object object) {

        if (object instanceof Handshake) {
            Handshake handshake = (Handshake)object;
            if (handshake.type != Handshake.RESPONSE) return;

            ID = handshake.id;

            Main.window.gameScene.missions.append("Connected to " + connection.getRemoteAddressTCP().getHostName());

        }

        if (object instanceof Message) {
            Message msg = (Message)object;
            Main.window.gameScene.missions.append(msg.msg);
        }

        if (object instanceof TimeLimit) {
            TimeLimit time = (TimeLimit)object;
            Main.window.gameScene.time.setMaximum(time.millis);
            Main.window.gameScene.time.setMinimum(0);
            Main.window.gameScene.time.setValue(time.millis);
        }

        if (object instanceof Quit) {
            Main.window.switchToMainMenu();
        }

    }


}
