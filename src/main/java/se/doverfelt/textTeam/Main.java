/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam;

import com.esotericsoftware.minlog.Log;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;
import se.doverfelt.textTeam.gui.LWJGLWindow;
import se.doverfelt.textTeam.gui_old.Window;

import javax.swing.*;

public class Main {

    public static final String VERSION = "0.1";

    private static Thread windowT;

    public static Window window;

    public static LWJGLWindow lwjglWindow;

    public static void main(String args[]) {

        if  (args.length > 0 && args[0].equals("lwjgl")) {
            try {
                lwjglWindow = new LWJGLWindow("TextTeam " + VERSION);
                AppGameContainer appgc = new AppGameContainer(lwjglWindow);
                //appgc.setIcon("/res/gasolgasoft.png");
                appgc.setTargetFrameRate(60);
                appgc.setVSync(true);
                //appgc.setFullscreen(true);
                appgc.setDisplayMode(appgc.getScreenWidth(), appgc.getScreenHeight(), true);
                //appgc.setDisplayMode(1600, 900, true);
                appgc.start();
            } catch (SlickException e) {
                e.printStackTrace();
            }
            return;
        }

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        Log.set(Log.LEVEL_DEBUG);

        window = new Window();
        windowT = new Thread(window);
        windowT.start();

    }

}
