
/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui_old;

import se.doverfelt.textTeam.Main;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ConnectMenu extends JPanel {

    private final BoxLayout layout;
    private final JTextField hostname;
    private final StyledButton join;
    private BufferedImage logo;

    public ConnectMenu() {

        try {
            logo = ImageIO.read(ConnectMenu.class.getResourceAsStream("/res/logo.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        layout = new BoxLayout(this, BoxLayout.LINE_AXIS);
        //layout.setVgap(10);

        setLayout(layout);
        setBackground(Color.BLACK);

        //logo = new JLabel(new ImageIcon(MainMenu.class.getResource("res/logo.png")));
        join = new StyledButton("Connect");
        hostname = new JTextField();
        hostname.setPreferredSize(new Dimension(150, 20));
        hostname.setMaximumSize(new Dimension(250, 30));
        hostname.setBackground(Color.BLACK);
        hostname.setForeground(Color.decode("#339933"));
        hostname.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

        hostname.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.window.switchToGame(hostname.getText());
            }
        });

        join.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.window.switchToGame(hostname.getText());
            }
        });

        //add(logo);
        add(Box.createHorizontalGlue());
        add(Box.createVerticalGlue());
        add(hostname);
        add(Box.createRigidArea(new Dimension(10, 10)));
        add(join);
        add(Box.createHorizontalGlue());
        add(Box.createVerticalGlue());
    }

    public void enableScene() {
        hostname.grabFocus();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(logo, 0, 0, getWidth(), getHeight(), null);
    }

}
