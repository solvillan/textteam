/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui_old;

import se.doverfelt.textTeam.Main;
import se.doverfelt.textTeam.client.ClientSide;
import se.doverfelt.textTeam.server.GameServer;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Window extends JFrame implements Runnable {

    private MainMenu mainMenu;
    public GameScene gameScene;
    public ConnectMenu connectMenu;
    public ClientSide clientSide;
    public GameServer gameServer;
    public Thread gameLogicT;

    private JPanel contentPane;

    private int timer = 0;
    private boolean renderLogo = true;

    private Image logo = new ImageIcon(Window.class.getResource("/res/gasolgasoft.png")).getImage();

    @Override
    public void run() {
        while (true) {
            if (gameScene.time.getMaximum() > 0) {
                gameScene.time.setValue(gameScene.time.getValue() - 1);
                gameScene.time.setString(Math.round((float)gameScene.time.getValue() / (float)gameScene.time.getMaximum() * 100f) + "%");
                gameScene.time.setStringPainted(false);
            } else {
                gameScene.time.setStringPainted(false);
            }
            gameScene.time.repaint();
            repaint();
            if (timer >= 3000) {
                renderLogo = false;
            } else {
                timer++;
            }

            try {
                Thread.sleep(1);
                Thread.yield();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.setColor(Color.black);
        if (renderLogo) {
            g2.drawImage(logo, (this.getWidth()/2)-((this.getHeight()-25)/4), ((this.getHeight()-25)/4), (this.getHeight()-25)/2,(this.getHeight()-25)/2, null);
        } else {
            super.paint(g2);
        }
    }

    public Window() {

        setSize(800, 600);
        setLocationByPlatform(true);
        setLocationRelativeTo(null);
        setTitle("TextTeam " + Main.VERSION);
        setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        setIconImage(logo);

        setBackground(Color.BLACK);
        setForeground(Color.GREEN);

        contentPane = new JPanel();
        contentPane.setBackground(Color.BLACK);
        contentPane.setForeground(Color.GREEN);
        contentPane.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        contentPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        contentPane.setLayout(new CardLayout());

        gameScene = new GameScene();
        connectMenu = new ConnectMenu();
        mainMenu = new MainMenu();

        contentPane.add(mainMenu);
        contentPane.add(connectMenu);
        contentPane.add(gameScene);

        this.setContentPane(contentPane);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    public void switchToGame(String ip) {

        CardLayout layout = (CardLayout) contentPane.getLayout();
        layout.last(contentPane);
        try {
            clientSide = new ClientSide(ip);
        } catch (IOException e) {
            e.printStackTrace();
        }
        gameScene.enableScene();

    }

    public void switchToConnect() {

        CardLayout layout = (CardLayout) contentPane.getLayout();
        layout.next(contentPane);

    }

    public void switchToMainMenu() {
        CardLayout layout = (CardLayout) contentPane.getLayout();
        layout.first(contentPane);
    }

}
