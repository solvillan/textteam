/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui_old;

import se.doverfelt.textTeam.Main;
import se.doverfelt.textTeam.client.ClientLogic;
import se.doverfelt.textTeam.packets.Message;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class GameScene extends JPanel {

    private BufferedImage logo;
    public JTextArea missions;
    private JTextField input;
    public JProgressBar time;
    private JScrollPane scrollPane;
    private BorderLayout layout;
    private Color transparentBlack = new Color(0,0,0,230);

    public void enableScene() {
        input.grabFocus();
    }

    public GameScene() {
        try {
            logo = ImageIO.read(ConnectMenu.class.getResourceAsStream("/res/logo.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.setBackground(Color.BLACK);
        this.setForeground(Color.decode("#339933"));
        missions = new JTextArea() {
            @Override
            public void append(String in) {
                super.append(in + "\n");
                scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
            }
        } ;
        input = new JTextField();
        time = new JProgressBar(JProgressBar.HORIZONTAL){
            @Override
            public void paint(Graphics g) {
                g.setColor(transparentBlack);
                g.fillRect(time.getX(), time.getY(), time.getWidth()-1, time.getHeight()-1);

                g.setColor(Color.RED);
                g.drawRect(time.getX(), time.getY(), time.getWidth()-1, time.getHeight()-1);


                if (time.getValue() != 0) {
                    int progress = (time.getWidth()-3)*(time.getValue() / time.getMaximum())*10000;
                    g.setColor(Color.decode("#cecec6"));
                    g.fill3DRect(time.getX() + 2, time.getY() + 2, (int) (time.getPercentComplete()*(time.getWidth()-4)), time.getHeight() - 4, true);
                }

                //super.paint(g);
            }
        };
        scrollPane = new JScrollPane(missions);
        scrollPane.setAutoscrolls(true);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);


        time.setPreferredSize(new Dimension(this.getWidth(), 50));
        missions.setEditable(false);
        missions.setFocusable(false);
        missions.setForeground(Color.decode("#339933"));
        missions.setBackground(new Color(0, 0, 0, 230));
        missions.setOpaque(true);
        missions.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 18));

        input.setBackground(Color.BLACK);
        input.setForeground(Color.decode("#339933"));
        input.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

        time.setBackground(Color.BLACK);
        time.setForeground(Color.decode("#339933"));

        layout = new BorderLayout(0, 5);
        setLayout(layout);

        input.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Message message = new Message();
                message.msg = input.getText();
                message.ID = ClientLogic.ID;
                Main.window.clientSide.getClient().sendTCP(message);
                message = null;
                input.setText("");
                input.grabFocus();
            }
        });

        add(time, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
        add(input, BorderLayout.SOUTH);

        setVisible(true);

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(logo, 0, 0, getWidth(), getHeight(), null);
    }

}
