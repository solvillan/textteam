/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui_old;

import se.doverfelt.textTeam.Main;
import se.doverfelt.textTeam.server.GameServer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MainMenu extends JPanel {

    //private JLabel logo;
    private StyledButton host;
    private StyledButton join;
    private StyledButton quit;
    private BoxLayout layout;

    private BufferedImage logo;

    public MainMenu() {

        try {
            logo = ImageIO.read(ConnectMenu.class.getResourceAsStream("/res/logo.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        layout = new BoxLayout(this, BoxLayout.LINE_AXIS);
        //layout.setVgap(10);
        this.setBackground(Color.BLACK);
        this.setForeground(Color.GREEN);

        setLayout(layout);

        //logo = new JLabel(new ImageIcon(MainMenu.class.getResource("res/logo.png")));
        host = new StyledButton("Host a Game");
        join = new StyledButton("Join a Game");
        quit = new StyledButton("Quit Game");

        host.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    Main.window.gameServer = new GameServer();
                    Main.window.gameLogicT = new Thread(Main.window.gameServer.gameLogic);
                    Main.window.gameLogicT.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Main.window.switchToGame("127.0.0.1");
            }
        });

        join.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.window.switchToConnect();
                Main.window.connectMenu.enableScene();
            }
        });

        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });

        //add(logo);
        add(Box.createHorizontalGlue());
        add(host, BorderLayout.CENTER);
        add(Box.createRigidArea(new Dimension(10, 10)), BorderLayout.CENTER);
        add(join, BorderLayout.CENTER);
        add(Box.createRigidArea(new Dimension(10, 10)), BorderLayout.CENTER);
        add(quit, BorderLayout.CENTER);
        add(Box.createHorizontalGlue());
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(logo, 0, 0, getWidth(), getHeight(), null);
    }

}
