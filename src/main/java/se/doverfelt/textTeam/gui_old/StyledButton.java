/******************************************************************************
 * Copyright (c) 2014 Rickard Doverfelt                                       *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  *
 * copies of the Software, and to permit persons to whom the Software is      *
 * furnished to do so, subject to the following conditions:                   *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE*
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER     *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  *
 * THE SOFTWARE.                                                              *
 ******************************************************************************/

package se.doverfelt.textTeam.gui_old;

import javax.swing.*;
import java.awt.*;

public class StyledButton extends JButton {

    public StyledButton(String label) {
        super(label);
        setContentAreaFilled(false);
        setOpaque(true);
        setForeground(Color.green);
        setBackground(Color.BLACK);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.green);
        g.drawRect(0, 0, getWidth()-1, getHeight()-1);
    }

    @Override
    public void paintBorder(Graphics g) {
        //g.setColor(Color.green);
        //g.drawRect(getX(), getY(), getWidth(), getHeight());
    }

}
